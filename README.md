# Demo videos

This project contains two videos illustrating the work I have done in the recent past. These were built in a web application sold to our enterprise users.

### Dashboard

Simple example of how I was able to modernize the application using new frameworks

### Enterprise Search

This was a major feature that was released into production in 2015. There is a patenting pending on underlying technology, as well as the UI query builder. 

This feature allows people to build search queries, and scan their entire set of enterprise endpoints in a quick and easy fashion.


### Hackathon project

This project won 1st place at the FireEye Hackathon in 2015. I came up with the idea while working on Enterprise Search, assembled a team, and built the feature in 24 hours.

The "pivot" feature allows people to search for computers, that are statistically "similar" to ones that they have deemed interesting or significant. This allows them to find malware on computers that aren't displaying the exact same symptoms known infected hosts.

## Dashboard

Short video shows the dashboard I built. The animations you see are built using d3js. 


## Enterprise Search + Hackathon project

This video highlights two features I played a key role in.

### Enterprise Search: (Timestamp 0:00 - 0:10)


The first 10 seconds very briefly shows the enterprise search query builder. This is the novel ui used by security analysts to build queries that allows them to search across their endpoint network. It only shows a glimpse of the feature set, but this feature has a patent on it.


### Hit Review: (Timestamp 0:40 - 0:55)


The query results start streaming back with computers that hit an exact result. You see the ui moving around mimicking how a person analyze the results.


### Building Pivot Request: (Timestamp 0:55 - 1:06)


You see a person select the "pivot" option and fill out the form that allows them to choose the field from which a statistical model is created.


### Pivot Results: (Timestamp 1:08 - end)


You see the radar view appear and computers which didn't exactly match the Enterprise search query, but were deemed statistically close to the computers selected.










